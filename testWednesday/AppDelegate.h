//
//  AppDelegate.h
//  testWednesday
//
//  Created by Click Labs 105 on 11/4/15.
//  Copyright (c) 2015 Koshal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

