//
//  ViewController.h
//  testWednesday
//
//  Created by Click Labs 105 on 11/4/15.
//  Copyright (c) 2015 Koshal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>


@interface ViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *appleMap;
@end


