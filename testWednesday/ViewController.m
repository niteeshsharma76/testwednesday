//
//  ViewController.m
//  testWednesday
//
//  Created by Click Labs 105 on 11/4/15.
//  Copyright (c) 2015 Koshal. All rights reserved.
//

#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
@import GoogleMaps;

@interface ViewController (){
     CLLocationManager *manager;
    GMSMapView *googleMap;
    MKMapView *appleMap;
    UITextField *Longitude;
    UITextField *Latitude;
     UITextField *Name;
     UITextField *state;
     UITextField *city;
     UITextField *Country;
    UIButton *button;
    CLLocation *currentLocation;
    GMSMarker *marker;
    NSString *title;
    UILabel *lable;
}
@end
@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    googleMap = [GMSMapView mapWithFrame:CGRectMake(10, 10, 350, 300) camera:nil];
    googleMap.myLocationEnabled = YES;
    
    googleMap.delegate = self;
    [self.view addSubview:googleMap];
    
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
    
    
//    button=[[UIButton alloc]init];
//    button.frame=CGRectMake(20, 320, 100, 50);
//    button.backgroundColor=[UIColor redColor];
//   [button setTitle: @"My Button" forState: UIControlStateNormal];
//    [button addTarget:self action:@selector(location) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:button];
    
    
    lable=[[UILabel alloc]init];
    lable.frame=CGRectMake(100, 320, 150, 50);
    lable.text=@"Country";
    lable.backgroundColor=[UIColor greenColor];
    [self.view addSubview:lable];
    
    
    Name=[[UITextField alloc]init];
    Name.frame=CGRectMake(100, 450, 200, 20);
    Name.backgroundColor=[UIColor lightGrayColor];
    Name.placeholder=@"Name";
    [self.view addSubview:Name];

    state=[[UITextField alloc]init];
    state.frame=CGRectMake(100, 480, 200, 20);
    state.backgroundColor=[UIColor lightGrayColor];
    state.placeholder=@"State";
    [self.view addSubview:state];

    Country=[[UITextField alloc]init];
    Country.frame=CGRectMake(100, 510, 200, 20);
    Country.backgroundColor=[UIColor lightGrayColor];
    Country.placeholder=@"Country";
    [self.view addSubview:Country];

    
    
    Latitude=[[UITextField alloc]init];
    Latitude.frame=CGRectMake(100, 540, 200, 20);
    Latitude.backgroundColor=[UIColor lightGrayColor];
    Latitude.placeholder=@"latitude";
    [self.view addSubview:Latitude];
    
    Longitude=[[UITextField alloc]init];
    Longitude.frame=CGRectMake(100, 570, 200, 20);
    Longitude.backgroundColor=[UIColor lightGrayColor];
    Longitude.placeholder=@"longitude";
    [self.view addSubview:Longitude];
    
    
    
    }
-(void)getlocation:(id)sender {
    [manager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        Longitude.text = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.longitude];
        Latitude.text = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.latitude];
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                longitude:currentLocation.coordinate.longitude
                                                                     zoom:2];
        
        marker = [[GMSMarker alloc] init];
        marker.position = currentLocation.coordinate;
        
        marker.map = googleMap;
        googleMap.delegate = self;
        [self address:currentLocation.coordinate];
    }
}



-(void) address:(CLLocationCoordinate2D) currentLocation {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.latitude
                                                        longitude:currentLocation.longitude];
    
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@", addressDictionary);
                           NSString *city=addressDictionary[@"City"];
                    
                           if (city!=nil) {
                           state.text= [NSString stringWithFormat:@"%@",city];
                           }
                           else{
                              state.text= @"NA";
                           }
                           NSString *name=addressDictionary[@"Name"];
                           
                           if (name!=nil) {
                               Name.text= [NSString stringWithFormat:@"%@",name];
                           }
                           else{
                               state.text= @"NA";
                           }

                            NSString *country = addressDictionary[@"Country"];
                           if (country!=nil) {
                               Country.text= [NSString stringWithFormat:@"%@",country];
                               if ([Country.text isEqualToString:@"India"]) {
                                   lable.text=@"INDIA";
                               }
                               else{
                                   lable.text=@"OutSide india";
                               }
                                }
                           else{
                               Country.text= @"NA";
                           }
                          

                           
                           Longitude.text=[NSString stringWithFormat:@"%2f",currentLocation.longitude];
                            Latitude.text=[NSString stringWithFormat:@"%2f",currentLocation.latitude];

//                           marker.title = [NSString stringWithFormat:@"%@",name];
//                           
//                           
//                           [manager stopUpdatingLocation];
                           

                       }
                       
                   }];
     }
     


- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = coordinate;
    
    marker.map = googleMap;
    // googleMap.delegate = self;
    [self address:coordinate];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
